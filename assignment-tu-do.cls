\ProvidesClass{assignment-tu-do}[2015/12/20 v0.1 Assignments at TU Dortmund written by Florian Grieskamp]

%% Fallback
\DeclareOption*{
  \PassOptionsToClass{\CurrentOption}{scrartcl}
}

\ProcessOptions\relax

\LoadClass[11pt, a4paper, oneside]{scrartcl}

\RequirePackage[top=1in, right=1in, bottom=1in, left=1in, includeheadfoot]{geometry}
\RequirePackage[ngerman]{babel}
\RequirePackage{amsmath,amsthm,amsfonts,amssymb,wasysym}
\RequirePackage{listings}
\RequirePackage{xcolor}
\RequirePackage[headsepline=1pt]{scrlayer-scrpage}
\RequirePackage{iflang}
\RequirePackage{graphicx}

%+--------------------------------------------------------------------+
%|                                                                    |
%|                         Initialization                             |
%|                                                                    |
%+--------------------------------------------------------------------+

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codeblue}{rgb}{0.0,0,0.8}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
\definecolor{tu}{rgb}{0.431,0.78,0.176}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codeblue},
    basicstyle=\ttfamily,
    breakatwhitespace=true,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=left,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2
}

\lstset{style=mystyle}


%+--------------------------------------------------------------------+
%|                                                                    |
%|                             Key Data                               |
%|                                                                    |
%+--------------------------------------------------------------------+

    %--------------------------------------------------------------
    %--------------------Define assignment number------------------
    %--------------------------------------------------------------

    \def \thenumber {}
    \newcommand{\assignnumber}[1]{\def \thenumber {#1}}

    %--------------------------------------------------------------
    %--------------------Define course-----------------------------
    %--------------------------------------------------------------
            
    \def \thecourse {}
    \newcommand{\assigncourse}[1]{\def \thecourse {#1}}
                    
    %--------------------------------------------------------------
    %--------------------Define assignment group-------------------
    %--------------------------------------------------------------

    \def \thegroup {}
    \newcommand{\assigngroup}[1]{\def \thegroup {#1}}


%+--------------------------------------------------------------------+
%|                                                                    |
%|                        Document Structure                          |
%|                                                                    |
%+--------------------------------------------------------------------+

    %--------------------------------------------------------------
    %--------------------Define title------------------------------
    %--------------------------------------------------------------

    \title{\IfLanguageName{ngerman}{\"Ubungsblatt}{Assignment} \thenumber}

    %--------------------------------------------------------------
    %--------------------Maketitle---------------------------------
    %--------------------------------------------------------------
    
    \renewcommand{\maketitle}{
     \thispagestyle{plain}
     
     \begin{center}
      {\huge\bfseries \textsc{\@title} \hfill \raisebox{-1ex}{\includegraphics[height=3ex]{tu.png}}}
      \textcolor{tu}{\rule{\textwidth}{0.8pt}} 
      \begin{minipage}[t]{0.55\textwidth}
       {\bfseries
        \textsc{\thecourse} \\
        \textsc{\IfLanguageName{ngerman}{Gruppe}{Group} \thegroup}
       }
      \end{minipage}
      \hfill
      \begin{minipage}[t]{0.40\textwidth}
       \begin{flushright}
        \textsc{\@author}
       \end{flushright}
      \end{minipage}
     \end{center}
     
     \vspace{2\baselineskip}
    }

    %--------------------------------------------------------------
    %--------------------Define task-------------------------------
    %--------------------------------------------------------------

    \newcommand{\task}[1]{\subsection*{Aufgabe \thenumber .#1}}

    %--------------------------------------------------------------
    %--------------------Define abstask----------------------------
    %--------------------------------------------------------------

    \newcommand{\abstask}[1]{\subsection*{Aufgabe #1}}

    %--------------------------------------------------------------
    %--------------------Headers and footers-----------------------
    %--------------------------------------------------------------

    \ihead{\@title}
    \chead{\IfLanguageName{ngerman}{Gruppe}{Group} \thegroup}
    \ohead{\@author}

    \ifoot*{}
    \cfoot*{}
    \ofoot*{\IfLanguageName{ngerman}{Seite}{Page} \thepage}

    \addtokomafont{headsepline}{\color{tu}}

    \pagestyle{scrheadings}
    \setkomafont{pageheadfoot}{\small}

%+--------------------------------------------------------------------+
%|                                                                    |
%|                       Symbols and Functions                        |
%|                                                                    |
%+--------------------------------------------------------------------+

    %--------------------------------------------------------------
    %--------------------Define defeq------------------------------
    %--------------------------------------------------------------

    \newcommand{\defeq}{\stackrel{\textup{\tiny def}}{=}}

    %--------------------------------------------------------------
    %--------------------Define dx---------------------------------
    %--------------------------------------------------------------

    \newcommand{\dx}{~\mathrm{d}x}

    %--------------------------------------------------------------
    %--------------------Define vector drawing---------------------
    %--------------------------------------------------------------

    \newcommand{\drawvec}[1]{
	\left(\begin{array}{r}#1\end{array}\right)
    }

    %--------------------------------------------------------------
    %--------------------Define matrix drawing---------------------
    %--------------------------------------------------------------

    \newcommand{\drawmatrix}[2]{
	\left(\begin{array}{#1}#2\end{array}\right)
    }

    %--------------------------------------------------------------
    %--------------------Define dimension--------------------------
    %--------------------------------------------------------------

    \newcommand{\dimension}{
	\textup{dim}\,
    }

    %--------------------------------------------------------------
    %--------------------Define covariance-------------------------
    %--------------------------------------------------------------

    \newcommand{\cov}{
	\text{\sffamily cov}\,
    }

    %--------------------------------------------------------------
    %--------------------Define variance---------------------------
    %--------------------------------------------------------------

    \newcommand{\var}{
	\text{\sffamily var}\,
    }

    %--------------------------------------------------------------
    %--------------------Define determinant------------------------
    %--------------------------------------------------------------

    \newcommand{\determinant}[2]{
        \left|\begin{array}{#1}#2\end{array}\right|
    }

%+--------------------------------------------------------------------+
%|                                                                    |
%|                           Environments                             |
%|                                                                    |
%+--------------------------------------------------------------------+

    %--------------------------------------------------------------
    %--------------------Define definition environment-------------
    %--------------------------------------------------------------

    \newenvironment{definition}{
	\noindent \par \leftskip=2cm \rightskip=2cm \textsl\bgroup
    }{\egroup \par}

\endinput
